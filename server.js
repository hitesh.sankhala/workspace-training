
var http = require('http');
var reqCount = 0; 

var server = http.createServer( (req, res) => {
	console.log(">> new request received:"+req.url + " count:"+reqCount++);
	res.statusCode = 200;
	res.write('Hello World! count :'+reqCount);
	res.end();
});
server.listen(3000);
// curl http://localhost:3000/
// ab -n 10000 -c 100 http://localhost:3000/