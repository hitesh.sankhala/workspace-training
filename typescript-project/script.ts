const world = 'world';

function hey(word: string = world): string {
  return `Hello ${world}! `;
}

console.log(hey());