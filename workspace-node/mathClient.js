import { sum, multiple } from "./mathLib.js"
var math = require('./mathLib');

// console.log("sum of 4 + 6 = " + math.sum(4, 6));
// console.log("mul of 4 * 6 = " + math.mul(4, 6));

console.log("Sum(3+5) = " + sum(3, 5));
console.log("Multiple(3 * 5) = " + multiple(3, 5));