const express = require('express'); // not from node
const {getCustomers,addCustomer,getCustomerById,updateCustomer,deleteCustomer} = require('./customerService'); 

var app = express();

app.use(express.json());
app.get('/', function (req, res) {
	res.send('Hello World!');
});

app.get('/about', function (req, res) {
	res.send('Hello About!');
});

app.get('/api/customer', function (req, res) {
	res.send(getCustomers());
});

app.get('/api/customer/:id', function (req, res) {
	let customerId = req.params.id;
	res.send(getCustomerById(customerId));
});

app.put('/api/customer', function (req, res) {
	let customer = req.body;
	updateCustomer(customer);
	res.send({msg:'customer updated successfully', result:'success'});
});
app.delete('/api/customer', function (req, res) {
	let customer = req.body;
	deleteCustomer(customer);
	res.send({msg:'customer deleted successfully', result:'success'});
});

app.post('/api/customer', function (req, res) {
	let customer = req.body;
	addCustomer(customer);
	res.send({msg:'customer added successfully', result:'success'});
});

var server = app.listen(3000, function () {
console.log('Express app listening at http://localhost:3000');
});


// Method | url