
let customers = [
	{id:1,name:'Vivek', email:'vivek@abc.com', phone:'8989389333',address:"Singapore"},
	{id:2,name:'Dev', email:'dev@abc.com', phone:'866u389333',address:"India"},
	{id:3,name:'Ameer', email:'ameer@abc.com', phone:'877779333',address:"Asia"}
]; 

generateNextId= ()=>{
	let numberArray = customers.map(item=>{return item.id});
	console.log(numberArray);
	//spread operator
	let max = Math.max(...numberArray);
	return max +1;
}

const getCustomers = () =>(customers);
const addCustomer = (record) =>{
    record.id = generateNextId();
    customers.push(record);
};
const deleteCustomer =({id})=>{
    customers = customers.filter((item)=>(item.id!=id));
}
const getCustomerById=(id)=>{
    let temp = customers.filter((item)=>(item.id==id));
    if(temp.length > 0){
        return temp[0];
    }
    return {};
}
const updateCustomer=({id,name,email,phone,address})=>{
    let temp = customers.filter((item)=>(item.id==id));
    if(temp.length > 0){
        let record = temp[0];
        record.name = name;
        record.email = email;
        record.phone = phone;
        record.address = address;
    }
}
module.exports = {getCustomers,addCustomer,getCustomerById,deleteCustomer,updateCustomer};

